FROM alpine

RUN apk add --no-cache darkhttpd

EXPOSE 8080

USER darkhttpd:www-data

ENTRYPOINT [ "darkhttpd", "/var/www/localhost/htdocs", "--no-server-id" ]
